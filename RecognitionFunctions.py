import cv2, os

# ФУНКЦИЯ get_one_face загружает фотографию, ищет на ней лица, если оно одно, то возвращает его в сером цвете и
# пометку True, о том, что лицо найдено; если лиц нет или их больше 1, то считается, что лицо не найдено
# АРГУМЕНТЫ:
# path - путь до фотографии
# classifier_file - путь до файла классификатора, ищущего лица
# min_size - минимальный размер лица
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# пара (grey, is_found), где:
# grey - лицо в сером цвете или фотография полность, если лицо не было найдено
# is_found - True, если лицо было найдено
#            False, если нет
def get_one_face(path, classifier_file, min_size=(50, 50)):
    face_cascade = cv2.CascadeClassifier(classifier_file)
    grey = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(grey, 1.3, 5, minSize=min_size)
    if len(faces) == 1:
        is_found = True
        x, y, w, h = faces[0]
        grey = grey[y: y + h, x: x + w]
    else:
        is_found = False
    return grey, is_found


# ФУНКЦИЯ get_sets загружает фотографии, находит на них лица(использует get_one_face), возвращает лица и их маркеры
# АРГУМЕНТЫ:
# path - путь до папки, из которой функция загружает фотографии
# classifier_file - путь до файла классификатора, который ищет лицо
# persons - число людей, которых нужно загрузить
# photos_n - число фотографий на человека, которые нужно загрузить
# предполагается, что запрашивается всегда верное значение(не больше, чем имеется)
# min_size - минимальный размер лица
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# пара (photos, labels), где:
# photos - лица
# labels - маркеры
def get_sets(path='Output_input_files\\Photos',
             classifier_file='Output_input_files\\haarcascade_frontalface_default.xml',
             persons=10, photos_n=10, min_size=(50, 50)):
    photos = []
    labels = []
    persons_add = 0
    for id, (dir, subdirs, files) in enumerate(os.walk(path)):
        if len(files) < photos_n:
            continue
        persons_add = persons_add + 1
        label = int(dir.split('\\s')[-1])
        for file in files[0:photos_n]:
            if file.isspace(): continue
            photo, is_face = get_one_face(dir + '\\' + file, classifier_file, min_size)
            if is_face:
                photos.append(photo)
                labels.append(label)
        if persons <= persons_add:
            break
    return photos, labels


# ФУНКЦИЯ get_train_test_sets загружает фотографии и их маркеры, находит на них лица(использует get_one_face),
# делит лица на 2 части: обучающую(train) и тестовую(test) выборки (с маркерами!)
# АРГУМЕНТЫ:
# path - путь до папки, из которой функция загружает фотографии
# classifier_file - путь до файла классификатора, который ищет лицо
# ratio - отношение в котором данные будут делиться,
# значение ratio должно быть в пределах [0, 1], это вес обучающей выборки
# persons - количество людей, которое нужно загрузить
# photos - количество фотографий на человека, которое нужно загрузить
# предполагается, что запрашивается всегда верное значение(не больше, чем имеется)
# min_size - минимальный размер лица
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# набор (train_photos, train_labels, test_photos, test_labels), где:
# train_photos - лица обучающей выборки
# train_labels - ответы для обучающей выборки
# test_photos - лица тестовой выборки
# test_labels - ответы для тестовой выборки
def get_train_test_sets(path='Output_input_files\\Photos',
                        classifier_file='Output_input_files\\shape_predictor_68_face_landmarks.dat',
                        ratio=0.7, persons=9, photos=10, min_size=(50, 50)):
    train_photos = []
    test_photos = []
    train_labels = []
    test_labels = []

    # получим точные значения числа фотографий для обучения (tr) и тестирования (ts)
    tr = int(photos * ratio)
    ts = photos - tr
    if ts == 0:
        if tr > 1:
            ts = 1
            tr = tr - 1
    persons_add = 0
    for id, (dir, subdirs, files) in enumerate(os.walk(path)):
        if len(files) < photos:
            continue
        persons_add = persons_add + 1
        label = int(dir.split('\\s')[-1])
        for file in files[0:tr]:
            if file.isspace(): continue
            photo, is_face = get_one_face(dir + '\\' + file, classifier_file, min_size)
            if is_face:
                train_photos.append(photo)
                train_labels.append(label)
        if ts != 0:
            for file in files[tr:photos]:
                if file.isspace(): continue
                photo = cv2.imread(dir + '\\' + file)
                test_photos.append(photo)
                test_labels.append(label)
        if persons <= persons_add:
            break
    return train_photos, train_labels, test_photos, test_labels


# ФУНКЦИЯ get_predict_check распознает лицо на фотографии, с помощью обученного классификатора идентифицирует его,
# выводит на экран фотографию с отмеченным лицом и подписью: как лицо идентифицированно и правильно ли
# АРГУМЕНТЫ:
# photo - фотография
# lable - верный ответ
# trained_recognizer - обученный классификатор для идентификации лица
# classifier_file - путь до файла классификатора, который ищет лицо
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# нет
def get_predict_check(photo, lable, trained_recognizer,
                      classifier_file='Output_input_files\\haarcascade_frontalface_default.xml'):
    face_cascade = cv2.CascadeClassifier(classifier_file)
    grey = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(grey, 1.3, 5)
    for x, y, w, h in faces:
        prediction = trained_recognizer.predict(grey[y: y + h, x: x + w])
        cv2.rectangle(photo, (x, y), (x + w, y + h), (0, 225, 225), 2)
        cv2.rectangle(photo, (0, 0), (photo.size, 19
                                      ), (225, 225, 225), -1)
        cv2.putText(photo, "Pred is {}, lab is {}".format(prediction, lable), (10, 15),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
        if prediction[0] == lable:
            cv2.putText(photo, "True", (550, 15),
                        cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
            print("Right prediction: {}".format(prediction))
        else:
            cv2.putText(photo, "False", (550, 15),
                        cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
            print("!Wrong prediction: {}, true walue: {}".format(prediction, lable))


# ФУНКЦИЯ get_predict идентифицирует все уже найденные на изображении лица и
# выводит фотографию с именем и вероятностью, лица обводит в рамку
# АРГУМЕНТЫ:
# photo - фотография
# faces - расположение уже найденных лиц
# trained_recognizer - обученный классификатор для идентификации лица
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# нет
def get_predict(photo, faces, trained_recognizer):
    grey = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    for x, y, w, h in faces:
        prediction = trained_recognizer.predict(grey[y: y + h, x: x + w])
        cv2.rectangle(photo, (x, y), (x + w, y + h), (0, 225, 225), 2)
        cv2.rectangle(photo, (0, 0), (photo.size, 19), (225, 225, 225), -1)
        cv2.putText(photo, "prediction is {}, {}".format(prediction[0], prediction[1]), (10, 15),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
        if prediction[0] == -1:
            name = "Unknown"
        else:
            name = prediction[0]
        cv2.putText(photo, name, (x - 10, y - 10),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 225, 225), 1, cv2.LINE_AA)
