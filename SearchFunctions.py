import os
import cv2
import dlib
from imutils import face_utils
from imutils import resize

# ФУНКЦИЯ upload_photos загружает набор фотографий
# АРГУМЕНТЫ:
# path_to_photos - папка, из которой загружаются фотографии
# photos_need - количество фотографий, которое должно быть внутри 1 папки(те относится к 1 человеку), чтобы начать их загрузку
# persons_need - количество людей которое нужно, функция считает, сколько действительно загружено и возвращает это число
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# пара: (all_photos, real_persons_count), где
# all_photos - список загруженных фотографий вида [[фотографии_1_человека][фотографии_2_человека][...]...[...]]
# real_persons_count - действительное число людей, фотографии которых были загружены
def upload_photos(path_to_photos, persons_need=10, photos_need=4):
    all_photos = []
    real_persons_count = 0
    for id, (dir, subdirs, files) in enumerate(os.walk(path_to_photos)):
        if len(files) < photos_need:
            continue
        new_person = []
        for file in files[:photos_need]:
            if file.isspace(): continue
            new_photo = cv2.imread(dir + '\\' + file)
            new_person.append(new_photo)
        all_photos.append(new_person)
        real_persons_count = real_persons_count + 1
        if real_persons_count >= persons_need:
            break
    return (all_photos, real_persons_count)


# ФУНКЦИЯ output_photos поочереди выводит фотографии, новая появляется после закрытия старой
# АРГУМЕНТЫ:
# list_of_photos - список загруженных фотографий вида [[фотографии_1_человека][фотографии_2_человека][...]...[...]]
# number_of_persons - количество людей, которое необходимо вывести
# number_of_photos_per_person - количество фотографий на человека, которые нужно вывести
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# нет
def output_photos(list_of_photos, number_of_persons=5, number_of_photos_per_person=1):
    for i in range(number_of_persons):
        for j in range(number_of_photos_per_person):
            cv2.imshow('{} person, {} photo'.format(i + 1, j + 1), list_of_photos[i][j])
            cv2.waitKey()
    cv2.destroyAllWindows()


# ФУНКЦИЯ find_face_on_photo_dlib используя библиотеку dlib обнаруживает все лица на изображении,
# а также 68 точек, описывающих эти лица
# АРГУМЕНТЫ:
# photo - изображение на котором ищутся лица
# full_path_for_classifier_files - путь до специально обученного классификатора
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# rects - пара: первая переменная - количество лиц, найденных на фотографии
#               вторая - координаты прямоугольников(вида ((x1, y1), (x2, y2)) )в которых эти лица содержатся
# shapes - список из упорядоченных наборов по 68 точек на каждое лицо
def find_face_on_photo_dlib(photo, is_shapes=True,
                            classifier_file='Output_input_files\\shape_predictor_68_face_landmarks.dat'):
    grey = cv2.cvtColor(resize(photo, width=500), cv2.COLOR_BGR2GRAY)  # получаем серое изображение
    # grey = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)  # получаем серое изображение

    height, width, channels = photo.shape
    print(width)
    new_height, new_width = grey.shape
    new_height = height / new_height

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(classifier_file)

    rects1 = detector(grey, 1)  # сразу 2 переменные: колество найденных лиц и список координат углов прямоугольнико
    rects = []
    if is_shapes:
        shapes = []
        for (i, rect) in enumerate(rects1):
            shape1 = face_utils.shape_to_np(predictor(grey, rect))
            shape = []
            for (x, y) in shape1:
                x = int(x * new_height)
                y = int(y * new_height)
                shape.append([x, y])
            shapes.append(shape)
            (x, y, w, h) = face_utils.rect_to_bb(rect)
            rects.append([round(x * new_height), round(y * new_height), round(w * new_height), round(h * new_height)])
        return rects, shapes
    else:
        for (i, rect) in enumerate(rects1):
            (x, y, w, h) = face_utils.rect_to_bb(rect)
            rects.append((round(x * new_height), round(y * new_height), round(w * new_height), round(h * new_height)))
    return rects


# ФУНКЦИЯ draw_rectangles используя библиотеку OpenCV редактирует изображение, рисуя на нем заданные прямоугольники
# АРГУМЕНТЫ:
# photo - само изображение
# rects - список координат прямоугольников вида [(x, y, w, h), (...), ...],
#                   где x, y - координаты верхнего левого угла, w - ширина (по оси Х), h - высота (по оси Y)
# name - список названий, коотрые будут подписаны над прямоугольником
# color - цвет, которым отрисовываются стороны прямоугольника
# thickness - толщина сторон
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# нет
def draw_rectangles(photo, rects, name, color=(0, 225, 225), thickness=2):
    n_name = 0
    for (x, y, w, h) in rects:
        cv2.rectangle(photo, (x, y), (x + w, y + h), color, thickness)
        cv2.putText(photo, name[n_name], (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)
        n_name = n_name + 1


# ФУНКЦИЯ draw_landmark редактирует изображение, рисуя на нем заданные точки
# АРГУМЕНТЫ:
# photo - само изображение
# shapes - список координат всех точек
# color - цвет, которым отрисовываются окружности
# radius - радиус окружностей
# thickness - толщина линии, если отрицательная, то окружность закрашивается
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# нет
def draw_landmark(photo, shapes, color=(0, 225, 225), radius=1, thickness=-1):
    for shape in shapes:
        for (x, y) in shape:
            cv2.circle(photo, (x, y), radius, color, thickness)


# ФУНКЦИЯ find_faces_on_photo_cv2 используя библиотеку cv2 обнаруживает лица на изображении
# АРГУМЕНТЫ:
# photo - изображение на котором ищутся лица
# classifier_file - путь до специально обученного классификатора
# ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ:
# rects - пара: первая переменная - количество лиц, найденных на фотографии
#               вторая - координаты прямоугольников(вида ((x1, y1), (x2, y2)) )в которых эти лица содержатся
def find_faces_on_photo_cv2(photo, classifier_file='Output_input_files\\haarcascade_frontalface_default.xml'):
    face_cascade = cv2.CascadeClassifier(classifier_file)
    gray = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    return face_cascade.detectMultiScale(gray, 1.3, 5)
