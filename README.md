# OpenCV example

### File SearchFanctions.py :

Includes functions which use libraries os, cv2, dlib and imutils for:
* downloading the images 
* outputting the images
* face detection (both variants with dlib and cv2)
* marking face on images


### File RecognitionFunctions.py :

Includes functions which use libraries cv2 and os for:
* downloading test samples
* face recognition classifier training
* classifier usage

### Additional files

These functions require the folder called '\\Output_input_files' as a default, which includes:

* folder '\\Photos' with the training sample (for example, [Georgia Tech face database](http://www.anefian.com/research/face_reco.htm))
* trained classifiers for face detection (for example, ['\\haarcascade_frontalface_default.xml'](https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml) for cv2 and
['\\shape_predictor_68_face_landmarks.dat'](http://dlib.net/files/) for dlib)


